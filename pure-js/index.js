const Pinch = {
  out: 'out',
  in: 'in',
};

const Rotation = {
  clockwise: 'clock',
  counterclockwise: 'counterclockwise',
};

class Gestures {
  constructor({ element, onPinch, onRotate, onTranslate }) {
    this.pointers = new Map();
    this.element = element;
    this.distance = undefined;

    this.element.style = 'touch-action:none';

    this.element.onpointerdown = this.#onPointerDown.bind(this);
    this.element.onpointermove = this.#onPointerMove.bind(this);

    this.element.onpointerup = this.#onPointerUp.bind(this);
    this.element.onpointercancel = this.#onPointerUp.bind(this);
    this.element.onpointerout = this.#onPointerUp.bind(this);
    this.element.onpointerleave = this.#onPointerUp.bind(this);

    this.onPinch = onPinch;
    this.onRotate = onRotate;
    this.onTranslate = onTranslate;
  }

  #onPointerDown(event) {
    this.pointers.set(event.pointerId, event);
  }

  #onPointerUp(event) {
    this.pointers.delete(event.pointerId);
    if (this.pointers.size < 2) {
      this.distance = undefined;
      this.angle = undefined;
    }
  }

  #onPointerMove(event) {
    const prevSamePointerEvent = this.pointers.get(event.pointerId);
    if (prevSamePointerEvent !== undefined) {
      this.pointers.set(event.pointerId, event);
    }
    this.#translate(prevSamePointerEvent, event);

    if (this.pointers.size != 2) {
      return;
    }

    const prevDistance = this.distance;
    const prevAngle = this.angle;
    [this.distance, this.angle] = this.#calc();

    this.#pinch(prevDistance);
    this.#rotate(prevAngle);

    this.distance = newDistance;
  }

  #translate(prevSamePointerEvent, event) {
    if (prevSamePointerEvent == undefined 
      || event == undefined
      || this.pointers.size != 1) {
      return;
    }
    const dX = event.clientX - prevSamePointerEvent.clientX;
    const dY = event.clientY - prevSamePointerEvent.clientY;
    this.onTranslate(dX, dY);
  }

  #pinch(prevDistance) {
    if (prevDistance !== undefined) {
      const diff = this.distance - prevDistance;
      if (prevDistance < this.distance) {
        this.onPinch(Pinch.out, diff);
      }
      if (prevDistance > this.distance) {
        this.onPinch(Pinch.in, diff);
      }
    }
  }

  #rotate(prevAngle) {
    if (prevAngle !== undefined) {
      const diff = this.angle - prevAngle;
      if (prevAngle < this.angle) {
        this.onRotate(Rotation.counterclockwise, diff);
      }
      if (prevAngle > this.angle) {
        this.onRotate(Rotation.clockwise, diff);
      }
    }
  }

  #calc() {
    if (this.pointers.size != 2) {
      throw new Error('There should be two pointers to calculate the distance');
    }
    const e = Array.from(this.pointers.values())
      .sort((a, b) => a.pointerId - b.pointerId);
    const dX = e[0].clientX - e[1].clientX;
    const dY = e[0].clientY - e[1].clientY;

    const distance = Math.hypot(dX, dY);
    const angle = Math.atan2(dY, dX);
    return [ distance, angle ];
  }
}

const FontSyle = {
  fill: 'fill',
  stroke: 'stroke',
};

const main = document.getElementById('main');
const textControl = document.getElementById('text-control');
const input = document.getElementById('text-input');
const download = document.getElementById('btn-download-img');
const alpha = document.getElementById('alpha');

const picker = document.getElementById('picker-input');
const pickerForm = document.getElementById('picker-form');
const pickerDialog = document.getElementById('picker-dialog');

const refresh = document.getElementById('btn-refresh');

const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
const img = new Image();

let width;
let height;

function loadState() {
  const stored = localStorage.getItem('state');
  if (stored !== null) {
    return JSON.parse(stored);
  }
  return {
    text: input.value,
    color: '#ffffff',
    alpha: alpha.value / 100,
    angle: 0, // radians
    // position in scalar of the image size
    position: {
      x: 0.5,
      y: 0.5,
    },
    font: {
      family: 'Poppins',
      style: FontSyle.fill,
      bold: true,
      size: 0.03, // scalar of the image height
    }
  };
}

const state = loadState();
input.value = state.text;

function downloadImage() {
  const link = document.createElement('a');
  const originalFilename = picker.files[0].name;
  const parts = originalFilename.split('.').slice(0, -1);
  parts.push('watermarked', 'jpg');
  link.download = parts.join('.');
  link.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
  link.click();
}
download.onclick = downloadImage;

function drawText({
  text,
  position,
  size,
  color,
  angle,
  alpha,
}) {
  ctx.save();
  // ctx.globalCompositeOperation = 'source-over'; // overlay - default
  // ctx.globalCompositeOperation = 'overlay'; // burn
  // ctx.globalCompositeOperation = 'lighter'; // highlight
  ctx.globalAlpha = alpha;
  ctx.translate(position.x, position.y);
  ctx.rotate(angle)
  ctx.fillStyle = color;
  ctx.font = `bold ${size}px Poppins, serif`;
  ctx.textAlign = 'center';
  ctx.fillText(text, 0, 15 / 2);
  ctx.restore();
}

function drawImage() {
  width = img.naturalWidth
  height = img.naturalHeight;

  canvas.width = width;
  canvas.height = height;
  ctx.drawImage(img, 0, 0);
}

function onStateChange() {
  drawImage();
  drawText({
    text: state.text,
    color: state.color,
    angle: state.angle,
    alpha: state.alpha,
    position: {
      x: state.position.x * width,
      y: state.position.y * height,
    },
    size: state.font.size * height,
  });
  window.localStorage.setItem('state', JSON.stringify(state));
}

picker.addEventListener('change', () => {
  if (picker.files.length == 0) {
    return;
  }
  img.src = URL.createObjectURL(picker.files[0]);
  pickerDialog.close();
});

img.addEventListener('load', () => {
  picker.value = '';
  pickerForm.reset();
  onStateChange();
});

input.addEventListener('input', (e) => {
  state.text = e.target.value;
  onStateChange();
})

canvas.addEventListener('click', () => {
  textControl.showModal();
  input.selectionStart = input.selectionEnd = input.value.length;
});

alpha.addEventListener('input', () => {
  state.alpha = alpha.value / 100;
  onStateChange();
})

const gestures = new Gestures({
  element: canvas,
  onPinch: (_, distance) => {
    const newSize = state.font.size + distance / canvas.offsetHeight;
    state.font.size = Math.max(newSize, 0.01);
    onStateChange();
  },
  onRotate: (_, angle) => {
    state.angle += angle;
    onStateChange();
  },
  onTranslate: (dX, dY) => {
    if ((dX > 0 && state.position.x < 0.99)
      || (dX < 0 && state.position.x > 0.01)
    ) {
      state.position.x += dX / canvas.offsetWidth;
    }
    if ((dY > 0 && state.position.y < 0.99)
      || (dY < 0 && state.position.y > 0.01)
    ) {
      state.position.y += dY / canvas.offsetHeight;
    }
    onStateChange();
  },
});

function start() {
  textControl.close();
  pickerDialog.showModal();
}

refresh.onclick = start;
start();
