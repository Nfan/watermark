import { children, JSX } from 'solid-js';
import './Button.css';

type Props = {
  children: JSX.Element;
  backgroundColor: string;
  disabled?: boolean;
  autofocus?: boolean;
  onClick?: (event: MouseEvent) => void;
};

export default function Button(props: Props) {
  const c = children(() => props.children);
  return <button
    disabled={props.disabled}
    autofocus={props.autofocus}
    class="button"
    style={{
      "background-color": props.backgroundColor,
    }}
    onClick={props.onClick}>
    {c()}
  </button>
}
