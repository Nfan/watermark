import { Download, Opacity, RestartAlt } from '@suid/icons-material';
import { batch, createEffect, createSignal, on, onMount } from 'solid-js';
import { Gestures } from '~/lib/Gestures';
import { DrawableFile } from '~/lib/DrawableFile';
import SliderControl from './SliderControl';
import './Editor.css';
import OverlayControls from './OverlayControls';
import * as drawing from '~/lib/drawing';
import Button from './Button';
import PageControl from './PageControl';
import Loading from './Loading';
import { TextProps } from '~/lib/TextProps';
import { PDFDocument, radians, rgb } from 'pdf-lib';
import fontkit from '@pdf-lib/fontkit';
import fontUrl from '/Poppins-Regular.ttf';
import { createStore, unwrap } from 'solid-js/store';

type Props = {
  file: DrawableFile;
  exit(): void;
};

function download(url: string, filename: string) {
  const link = document.createElement('a');
  link.download = filename;
  link.href = url;
  link.click();
}

function downloadImage(canvas: HTMLCanvasElement, originalName: string) {
  const filename = [originalName, 'watermarked', 'jpg'].join('.');
  const url = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
  download(url, filename);
}

async function downloadPDF(file: File, originalName: string, text: TextProps) {
  const filename = [originalName, 'watermarked', 'pdf'].join('.');

  const arrayBuffer = await file.arrayBuffer();
  const pdf = await PDFDocument.load(arrayBuffer);

  pdf.registerFontkit(fontkit);
  const fontBytes = await fetch(fontUrl).then(res => res.arrayBuffer());
  const font = await pdf.embedFont(fontBytes);

  const angle = -text.angle

  for (const page of pdf.getPages()) {
    const { width, height, x, y } = page.getCropBox();
    const size = text.size * height;
    const textWidth = font.widthOfTextAtSize(text.text, size);
    page.drawText(text.text, {
      x: text.position.x * width - (textWidth * Math.cos(angle)) / 2 + x,
      y: (1 - text.position.y) * height - (textWidth * Math.sin(angle)) / 2 + y,
      size: text.size * height,
      font: font,
      color: rgb(0, 0, 0),
      opacity: text.opacity,
      lineHeight: 0,
      rotate: radians(angle),
    });
  }

  const pdfBytes = await pdf.save();
  const blob = new Blob([pdfBytes], { type: 'application/pdf' });
  const url = URL.createObjectURL(blob);
  download(url, filename);
}

function getColorForFileType(type: string): string {
  if (type === 'application/pdf') {
    return '#000000';
  } else {
    return '#ffffff';
  }
}

export default function Editor({ file, exit }: Props) {
  let canvas!: HTMLCanvasElement;

  let [currentPageCanvas, setCurrentPageCanvas] = createSignal<HTMLCanvasElement>();
  const currentPageNumberSignal = createSignal(1);
  const [currentPageNumber] = currentPageNumberSignal;
  const [isLoadingPage, setLoadingPage] = createSignal(false);

  const overlayOpenSignal = createSignal(false);
  const [_, setOverlayOpen] = overlayOpenSignal;

  const storedWatermark = window.localStorage.getItem('watermark');
  const [watermark, setWatermark] = createStore(
    storedWatermark
      ? JSON.parse(storedWatermark)
      : {
        text: 'Watermark',
        angle: 0,
        position: {
          x: 0.5,
          y: 0.5,
        },
        size: 0.05,
        opacity: 1,
        color: getColorForFileType(file.type),
      });

  const openOverlay = () => setOverlayOpen(true);
  const download = () => {
    const unwrappedWatermark = unwrap(watermark);
    window.localStorage.setItem('watermark', JSON.stringify(unwrappedWatermark));
    if (file.type === 'application/pdf') {
      downloadPDF(file.file, file.name, unwrappedWatermark);
    } else {
      downloadImage(canvas, file.name);
    }
  };

  let pageChangeTimeout: ReturnType<typeof setTimeout>;
  createEffect(on(currentPageNumber, (currentPageNumber) => {
    clearTimeout(pageChangeTimeout);
    pageChangeTimeout = setTimeout(() => {
      setLoadingPage(true);
      file.draw(currentPageNumber)
        .then((canvas) => {
          batch(() => {
            setCurrentPageCanvas(canvas)
            setLoadingPage(false);
          });
        });
    }, 200);
  }));

  createEffect(() => {
    const currentPage = currentPageCanvas();
    if (currentPage === undefined) {
      return;
    }
    drawing.drawImage(canvas, currentPage);
    drawing.drawText(canvas, watermark);
  });

  onMount(() => {
    new Gestures({
      element: canvas,
      onPinch: (_, distance) => {
        const newSize = watermark.size + distance / canvas.offsetHeight;
        setWatermark('size', Math.max(newSize, 0.01));
      },
      onRotate: (_, a) => {
        setWatermark('angle', watermark.angle + a)
      },
      onTranslate: (dX, dY) => {
        if ((dX > 0 && watermark.position.x < 0.99)
          || (dX < 0 && watermark.position.x > 0.01)
        ) {
          setWatermark('position', 'x', watermark.position.x + dX / canvas.offsetWidth);
        }
        if ((dY > 0 && watermark.position.y < 0.99)
          || (dY < 0 && watermark.position.y > 0.01)
        ) {
          setWatermark('position', 'y', watermark.position.y + dY / canvas.offsetHeight);
        }
      },
    });
  });

  return (<div class="editor">
    <OverlayControls
      openSignal={overlayOpenSignal}
      getText={() => watermark.text}
      setText={(text) => setWatermark('text', text)}
    >
      <Button backgroundColor="var(--clr-blue)" onClick={download}>
        <Download />
      </Button>
      <Button onClick={exit} backgroundColor="var(--clr-yellow)">
        <RestartAlt />
      </Button>
    </OverlayControls>
    <canvas
      ref={canvas}
      class="canvas"
      onClick={openOverlay}
    />
    {isLoadingPage() && <Loading />}
    <div class="controls">
      {file.numPages > 1 && <PageControl first={1} last={file.numPages} current={currentPageNumberSignal}/>}
      <SliderControl
        icon={Opacity}
        min={0}
        max={1}
        step={0.01}
        getValue={() => watermark.opacity}
        setValue={(value) => setWatermark('opacity', value)}
      />
    </div>
  </div>);
}
