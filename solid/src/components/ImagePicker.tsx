import { Setter } from "solid-js";
import './ImagePicker.css';

type Props = {
  setFile: Setter<File | undefined>;
};

export default function ImagePicker({ setFile }: Props) {
  function handleChange(event: any) {
    if (event == null || event.target == null) {
      return;
    }
    const input = event.target;
    if (input.files.length > 0) {
      setFile(input.files[0]);
    }
  }

  return (
    <input class="picker" type="file" accept="image/*,.pdf" onChange={handleChange}/>
  );
}
