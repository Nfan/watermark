import { Refresh } from '@suid/icons-material';
import { createSignal, onMount } from 'solid-js';
import './Loading.css';

export default function Loading() {
  const [start, setStart] = createSignal(false);

  onMount(() => {
    setTimeout(() => setStart(true), 0);
  })

  return <div classList={{
    loading: true,
    appear: start(),
  }}>
    <Refresh fontSize='large' class='spinning'/>
  </div>;
}
