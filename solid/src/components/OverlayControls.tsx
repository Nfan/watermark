import { Check } from "@suid/icons-material";
import { children, createEffect, JSX, Signal } from "solid-js";
import Button from "./Button";
import './OverlayControls.css';

type Props = {
  openSignal: Signal<boolean>;
  getText: () => string;
  setText: (text: string) => void;
  children: JSX.Element;
};

export default function OverlayControls(props: Props) {
  let modal: HTMLDialogElement | undefined = undefined;
  const {getText, setText} = props;
  const [open, setOpen] = props.openSignal;
  const c = children(() => props.children);

  const handleInput = (event: any) => {
    setText(event.target.value);
  };

  const handleModalClose = () => {
    setOpen(false);
  };

  createEffect(() => {
    if (modal === undefined) {
      return;
    }
    if (open() && !modal.open) {
      modal.showModal();
      return;
    }
    if (!open() && modal.open) {
      modal.close();
    }
  });

  return (<dialog ref={modal} onClose={handleModalClose} class="dialog">
    <form method="dialog">
      <input
        class="text-input"
        type="text"
        placeholder="Text"
        size="20"
        value={getText()}
        onInput={handleInput}
      />
      <Button backgroundColor="var(--clr-green)" autofocus>
        <Check />
      </Button>
      {c()}
    </form>
  </dialog>);
}
