import { ArrowBack, ArrowForward } from "@suid/icons-material";
import { Signal } from "solid-js";
import Button from "./Button";
import './PageControl.css';

type Props = {
  first: number;
  last: number;
  current: Signal<number>;
};

export default function PageControl(props: Props) {
  const [page, setPage] = props.current;

  const back = () => setPage(Math.max(props.first, page() - 1));
  const forward = () => setPage(Math.min(props.last, page() + 1));

  const isBackDisabled = () => page() === props.first;
  const isForwardDisabled = () => page() === props.last;

  return <div class="page-control">
    <Button
      disabled={isBackDisabled()}
      backgroundColor="var(--clr-blue)"
      onClick={back}>
      <ArrowBack />
    </Button>
    <div class="page-display">{page()} / {props.last}</div>
    <Button
      disabled={isForwardDisabled()}
      backgroundColor="var(--clr-blue)"
      onClick={forward}>
      <ArrowForward />
    </Button>
  </div>;
}
