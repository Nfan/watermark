import { SvgIcon } from "@suid/material";
import { Dynamic } from "solid-js/web";
import './SliderControl.css';

type Props = {
  icon: typeof SvgIcon;
  min: number;
  max: number;
  step: number;
  getValue: () => number;
  setValue: (value: number) => void;
};

export default function SliderControl(props: Props) {
  const {getValue, setValue} = props;

  const handleInput = (event: any) => {
    setValue(Number(event.target.value));
  };
  return (<div class="control">
    <Dynamic component={props.icon}/>
    <input
      type="range"
      min={props.min}
      max={props.max}
      step={props.step}
      value={getValue()}
      onInput={handleInput}
    />
  </div>);
}
