import * as drawing from '~/lib/drawing';
import * as PDF from 'pdfjs-dist';
import PDFWorker from 'pdfjs-dist/build/pdf.worker.mjs?worker';

PDF.GlobalWorkerOptions.workerPort = new PDFWorker();

export abstract class DrawableFile {
  file: File;
  filename: string;
  type: string;
  name: string;
  extension: string;
  numPages: number;

  constructor(file: File) {
    this.file = file;
    const splits = file.name.split('.');
    this.name = splits.slice(0, -1).join('.');
    this.extension = splits[splits.length - 1];
    if (file.type === 'application/pdf') {
      this.numPages = -1;
    } else {
      this.numPages = 1;
    }
    this.filename = file.name;
    this.type = file.type;
  }

  abstract draw(page: number): Promise<HTMLCanvasElement>;
}

export class ImageFile extends DrawableFile {
  draw(page: number): Promise<HTMLCanvasElement> {
    if (page < 1 || page > this.numPages) {
      const error = new Error(`Page number {page} is not in [1, ${this.numPages}]`);
      return Promise.reject(error);
    }
    return new Promise((resolve, reject) => {
      const canvas = document.createElement('canvas');
      const ctx = canvas.getContext('2d');
      if (ctx === null) {
        return reject(new Error("Couldn't acquire a 2d context from canvas"));
      }
      const image = new Image();
      image.addEventListener('load', () => {
        drawing.drawImage(canvas, image);
        resolve(canvas);
      });
      image.src = URL.createObjectURL(this.file);
    });
  }
}


export class PDFFile extends DrawableFile {
  pdf: PDF.PDFDocumentProxy;

  constructor(file: File, pdfProxy: PDF.PDFDocumentProxy) {
    super(file);
    this.pdf = pdfProxy;
    this.numPages = this.pdf.numPages;
  }

  async draw(page: number): Promise<HTMLCanvasElement> {
    if (page < 1 || page > this.numPages) {
      return Promise.reject(`Page number ${page} is outside of [1, ${this.numPages}]`)
    }
    const canvas = document.createElement('canvas');
    return this.pdf.getPage(page)
      .then(page => {
        const viewport = page.getViewport({ scale: 1 });
        canvas.width = viewport.width;
        canvas.height = viewport.height;
        const ctx = canvas.getContext('2d')!;

        return page.render({
          canvasContext: ctx,
          viewport: viewport,
        }).promise;
      })
      .then(() => canvas);
  }
}

export async function makeDrawableFile(file: File): Promise<DrawableFile> {
  if (file.type === 'application/pdf') {
    const arrayBuffer = await file.arrayBuffer();
    const pdf = await PDF.getDocument(arrayBuffer).promise;
    return new PDFFile(file, pdf);
  } else {
    return new ImageFile(file);
  }
}
