enum Pinch {
  IN,
  OUT,
}

enum Rotation {
  CLOCKWISE,
  COUNTERCLOCKWISE,
};

type Props = {
  element: HTMLElement;
  onPinch: (pinch: Pinch, distance: number) => void;
  onRotate: (rotation: Rotation, angle: number) => void;
  onTranslate: (dX: number, dY: number) => void;
};

export class Gestures {
  element: HTMLElement;
  pointers: Map<number, PointerEvent>;
  distance: undefined | number;
  angle: undefined | number;

  onPinch: (pinch: Pinch, distance: number) => void;
  onRotate: (rotation: Rotation, angle: number) => void;
  onTranslate: (dX: number, dY: number) => void;

  constructor({ element, onPinch, onRotate, onTranslate }: Props) {
    this.pointers = new Map();
    this.element = element;
    this.distance = undefined;

    this.element.style.cssText = 'touch-action:none';

    this.element.onpointerdown = this.#onPointerDown.bind(this);
    this.element.onpointermove = this.#onPointerMove.bind(this);

    this.element.onpointerup = this.#onPointerUp.bind(this);
    this.element.onpointercancel = this.#onPointerUp.bind(this);
    this.element.onpointerout = this.#onPointerUp.bind(this);
    this.element.onpointerleave = this.#onPointerUp.bind(this);

    this.onPinch = onPinch;
    this.onRotate = onRotate;
    this.onTranslate = onTranslate;
  }

  #onPointerDown(event: PointerEvent) {
    this.pointers.set(event.pointerId, event);
  }

  #onPointerUp(event: PointerEvent) {
    this.pointers.delete(event.pointerId);
    if (this.pointers.size < 2) {
      this.distance = undefined;
      this.angle = undefined;
    }
  }

  #onPointerMove(event: PointerEvent) {
    const prevSamePointerEvent = this.pointers.get(event.pointerId);
    if (prevSamePointerEvent !== undefined) {
      this.pointers.set(event.pointerId, event);
    }
    this.#translate(prevSamePointerEvent, event);

    if (this.pointers.size != 2) {
      return;
    }

    const prevDistance = this.distance;
    const prevAngle = this.angle;
    [this.distance, this.angle] = this.#calc();

    this.#pinch(prevDistance);
    this.#rotate(prevAngle);
  }

  #translate(prevSamePointerEvent: PointerEvent | undefined, event: PointerEvent) {
    if (prevSamePointerEvent == undefined 
      || event == undefined
      || this.pointers.size != 1) {
      return;
    }
    const dX = event.clientX - prevSamePointerEvent.clientX;
    const dY = event.clientY - prevSamePointerEvent.clientY;
    this.onTranslate(dX, dY);
  }

  #pinch(prevDistance: number | undefined) {
    if (prevDistance === undefined || this.distance === undefined) {
      return;
    }
    const diff = this.distance - prevDistance;
    if (prevDistance < this.distance) {
      this.onPinch(Pinch.OUT, diff);
    }
    if (prevDistance > this.distance) {
      this.onPinch(Pinch.IN, diff);
    }
  }

  #rotate(prevAngle: number | undefined) {
    if (prevAngle === undefined || this.angle === undefined) {
      return;
    }
    const diff = this.angle - prevAngle;
    if (prevAngle < this.angle) {
      this.onRotate(Rotation.COUNTERCLOCKWISE, diff);
    }
    if (prevAngle > this.angle) {
      this.onRotate(Rotation.CLOCKWISE, diff);
    }
  }

  #calc() {
    if (this.pointers.size != 2) {
      throw new Error('There should be two pointers to calculate the distance');
    }
    const e = Array.from(this.pointers.values())
      .sort((a, b) => a.pointerId - b.pointerId);
    const dX = e[0].clientX - e[1].clientX;
    const dY = e[0].clientY - e[1].clientY;

    const distance = Math.hypot(dX, dY);
    const angle = Math.atan2(dY, dX);
    return [ distance, angle ];
  }
}

