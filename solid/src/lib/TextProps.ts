import { Position } from "./Position";

export type TextProps = {
  color: string;
  text: string;
  opacity: number;
  position: Position;
  angle: number;
  size: number;
};


