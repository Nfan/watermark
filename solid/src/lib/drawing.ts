import { TextProps } from "./TextProps";

export function drawText(canvas: HTMLCanvasElement, text: TextProps) {
  const ctx = canvas.getContext('2d');
  if (ctx === null) {
    return;
  }
  ctx.save();
  // ctx.globalCompositeOperation = 'source-over'; // overlay - default
  // ctx.globalCompositeOperation = 'overlay'; // burn
  // ctx.globalCompositeOperation = 'lighter'; // highlight
  ctx.globalAlpha = text.opacity;
  ctx.translate(text.position.x * canvas.width, text.position.y * canvas.height);
  ctx.rotate(text.angle)
  ctx.fillStyle = text.color;
  const size = text.size * canvas.height;
  ctx.textAlign = 'center';
  ctx.font = `${size}px Poppins, serif`;
  ctx.fillText(text.text, 0, 0);
  ctx.restore();
}

export function drawImage(canvas: HTMLCanvasElement, image: HTMLImageElement | HTMLCanvasElement) {
  if (image instanceof HTMLImageElement) {
    canvas.width = image.naturalWidth;
    canvas.height = image.naturalHeight;
  } else {
    canvas.width = image.width;
    canvas.height = image.height;
  }
  const ctx = canvas.getContext('2d');
  if (ctx !== null) {
    ctx.drawImage(image, 0, 0);
  }
}

