import { createEffect, createSignal, Match, on, Switch } from "solid-js";
import Editor from "~/components/Editor";
import ImagePicker from "~/components/ImagePicker";
import Loading from "~/components/Loading";
import { DrawableFile, makeDrawableFile } from "~/lib/DrawableFile";
import { Stage } from "~/lib/Stage";

export default function Home() {
  const [stage, setStage] = createSignal(Stage.PickFile);
  const [file, setFile] = createSignal<File>();
  let drawableFIle: DrawableFile;

  const exit = () => setStage(Stage.PickFile);

  createEffect(on(file, (file) => {
    if (file === undefined) {
      return;
    }
    setStage(Stage.LoadingFile);
    makeDrawableFile(file)
      .then((dFile) => {
        drawableFIle = dFile;
        setStage(Stage.Editor);
      })
      .catch(() => setStage(Stage.PickFile));
  }));

  return (
    <main>
      <Switch>
        <Match when={stage() == Stage.PickFile}>
          <ImagePicker setFile={setFile}/>
        </Match>
        <Match when={stage() == Stage.LoadingFile}>
          <Loading />
        </Match>
        <Match when={stage() == Stage.Editor}>
          <Editor file={drawableFIle!} exit={exit}/>
        </Match>
      </Switch>
    </main>
  );
}
