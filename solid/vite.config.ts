import solid from "solid-start/vite";
import { defineConfig } from "vite";

export default defineConfig({
  plugins: [solid({ ssr: false })],
  server: {
    host: '0.0.0.0',
  },
  // PDF.js failing with "Top-level await.."
  // https://github.com/mozilla/pdf.js/issues/17245
  optimizeDeps: {
    esbuildOptions: {
      target: 'esnext',
    },
  },
});
